# Quicksort Timing

This project compares the timing of different Quicksort algorithm
implementations. It implements serial, OpenMP and OpenMPI implementations and
OpenMP and OpenMPI implementations of the Regular Sampling variation of the
Quicksort algorithm.

## Building
```
sudo apt install libopenmpi-dev
make
```

## Running
```
./sort -t -n 50
```

To see what flags are available, run `./sort --help`.
