\documentclass[sigconf, screen, nonacm]{acmart}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{subfig}
\usepackage{alltt}

\graphicspath{{./}{./build/}}

\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08emT\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

\copyrightyear{2019}
\setcopyright{none}

\begin{document}
\title{Parallel Sorting in High Performance Computing}
\author{Kyle Robbertze}
\email{RBBKYL001@myuct.ac.za}
\affiliation{%
  \institution{University of Cape Town}
  \streetaddress{Private Bag X3}
  \city{Cape Town}
  \country{South Africa}
  \postcode{7945}
}

\maketitle

\section{Introduction}
Sorting is an important problem in Computer Science. This is because it has both
theoretical interest and a variety of practical uses. There are many different
sorting algorithms, however most of these are focused around serial
computation~\cite{shi1992}. Parallel computing allows separate sections of the
problem to be done concurrently. This speeds up computation when the overhead of
running in parallel become trivial compared to the problem size.

Quicksort is an efficient sorting algorithm that breaks the sorting problem up
into several sub-sets. This is done recursively and each sub-set is sorted. Once
they are sorted, they are recombined iteratively until the final sorted list is
produced~\cite{hoare1962}. By breaking the sorting problem up and sorting each
sub-set individually, quicksort is a prime candidate for parallelising. However,
quicksort sorting performance is heavily impacted by the choice of pivot that is
used to split the data~\cite{hoare1962}.

Parallel sorting by regular sampling (PSRS) is a variant of quicksort that
ensures consistent performance over different number of nodes. This is done by
regularly sampling the data to select a pivot~\cite{shi1992}.

This report examines the implementations of these two algorithms using a shared
memory approach and using a message-passing approach. It seeks to answer the
question around which implementation performs better and what circumstances are
required for that to be the case. The shared memory approach is implemented
using OpenMP, a memory sharing framework for C, while the message-passing
implementation uses Open MPI, a C library for message-passing parallelisation.
All code is available on
GitLab\footnote{\url{https://gitlab.com/paddatrapper/quicksort-study}}, along
with the results and run scripts.

\section{Serial Quicksort}
The serial quicksort implementation sorts a list of integers from smallest to
largest sequentially. This is done by partitioning the list repeatedly.
Partitioning is done around a known pivot value. This value is known to be
within the list that is being partitioned. The list is scanned by two pointers
(one starting at the first item, and the other at the last item in the range).
The first pointer scans through the list until it reaches an item that is
greater than the pivot value. Then the second pointer moves down the list until
it reaches an item that is less than the pivot value. These two items are
exchanged and the pointers are moved one item in the appropriate direction. The
first pointer continues its scan and the process is repeated. This continues
until the pointers cross each other~\cite{hoare1962}.

The arrays timed in this study were generated randomly using the pseudorandom
number generator (PRNG) provided by the C standard library. While this does not
provide high entropy randomness, it is good enough for this purpose. Further, it
ensures that it is unlikely that the generated array is already ordered for
large lists. On average, serial quicksort has a complexity of
\[\mathcal{O}(n\log n)\]~\cite{hoare1962}. Thus, for 1 million items, it should
take
\[10^6\log 10^6 = 6\times10^6\text{ comparisons}\]
The University of Cape Town's High Performance Computing (HPC) cluster uses
2.2GHz AMD Opteron CPUs. On these one comparison takes 2 CPU
cycles~\cite{fog2011}. One cycle takes
\[\frac{1}{f} = T\implies\frac{1}{10^6} = 10^{-6}\text{s}\].
Thus 1 million items will theoretically take 6 seconds to complete.
\[t_{qs} = 6\times10^6\times10^{-6} = 6\text{s}\]

When implementing the serial quicksort, I used recursion to manage the stack of
sub-sets of the list. This reduces the complexity of the code, but can
potentially lead to large call stack sizes for large arrays. We avoided this by
only passing the recursive calls a pointer to the array, thus ensuring that each
call only added the minimum number of bytes to the stack.

Validity of my implementation was tested through execution of tests with
varying input sizes. The input sizes were chosen to match closely with the input
sizes expected during the timing of the algorithm. These tests verified that the
final array was sorted by comparing each item to the one before it. The output
of the tests can be found in Appendix~\ref{app:TestOutput}.

\section{OpenMP Parallel Algorithms}
\begin{figure*}[ht]
  \centering
  \subfloat[Quicksort using OpenMP for a range of input sizes. The number of
  threads is equal to the number of cores used for each plot.]{
    \includegraphics{build/omp}
    \label{fig:QOMP}
  }\quad\subfloat[PSRS using OpenMP on 2 and 4 threads for a range of input
  sizes.  The number of threads is equal to the number of cores used for each
  plot]{
    \includegraphics{build/romp}
    \label{fig:ROMP}
  }
  \caption{OpenMP implementations of quicksort and PSRS comparing relative
  speed-up when the number of cores and threads used changes}
  \label{fig:OMP}
\end{figure*}
We investigated two OpenMP parallel algorithms: quicksort and PSRS. The
quicksort algorithm imitated the serial implementation closely, except the
recursive calls were divided over the threads in the pool. The only difference
is the addition of a serial cut-off. Once the list is divided into sub-sets of
this length, the sub-sets are sorted sequentially. This reduces the parallel
overhead, as for small sizes of list, the overhead is large compared to the time
required to sort the list.

PSRS consists of three phases. In phase one, each thread sorts a sub-set of
the list of length \(\frac{n}{p}\) where \(n\) is the length of the list and
\(p\) is the number of threads. The threads then synchronize. From each of the
\(p\) lists, \(p - 1\) samples are chosen. In phase two these samples are sorted
using a sequential quicksort and from this ordered list \(Y_1, Y_2,\ldots,
Y_{p(p-1)}\), pivots \(Y_\frac{p}{2}, Y_{p + \frac{p}{2}},\ldots,Y_{(p - 2)p +
\frac{p}{2}}\) are chosen. This ensures that the pivots are evenly spaced
through the samples. These partitions are then sent to every processor and each
processor finds where each of the pivots divide its list. Then the processors
synchronize.

In the final phase, each processor uses a Mergesort to merge sections of all the
sub-sets that relate to its sub-set. This is done using the pivots found in
phase two, thus ensuring that all merges are independent of each other. Once
this is done and the processors synchronize, the list is sorted~\cite{shi1992}.

PSRS is \(\mathcal{O}\left(\frac{n}{p}\log n\right)\)~\cite{shi1992}. This means
that for 1 million items and 4 threads, PSRS performs
\[\frac{10^6}{4}\log10^6 = 1.5\times10^6\text{ comparisons}\]
Thus, the critical path will take
\[t_{rsomp} = 1.5\times10^6\times10^{-6} = 1.5\text{ s}\]

The speed-up over the serial quicksort implementation is
\[S_{rsomp} = \frac{t_s}{t_{rsomp}} = 4.0\]

The OpenMP quicksort implementation takes the same number of comparisons as the
serial implementation. However, it will be able to compute at most \(p\) of them
at a time. Thus, if there are 4 threads, the critical path will have
\[\frac{6}{4}\times10^6 = 1.5\times10^6\text{ comparisons}\]
Assuming there are 1 million items in the list, this will take 
\[t_{qomp} = 1.5\times10^6\times10^{-6} = 1.5\text{s}\]
There is also the cost of creating and managing the threads, which then
increases this running time, however with a sufficiently large problem size, the
overhead becomes negligible. Thus the speed-up over the serial quicksort
implementation is
\[S_{qomp} = \frac{t_s}{t_{qomp}} = 4.0\]

My OpenMP PSRS implementation was based on work by Silva and Malhadas
(2017)~\cite{silva2017}.

Validity of my implementation was tested through execution of tests with
varying input sizes. The input sizes were chosen to match closely with the input
sizes expected during the timing of the algorithm. These tests verified that the
final array was sorted by comparing each item to the one before it. The output
of the tests can be found in Appendix~\ref{app:TestOutput}.

\section{MPI Parallel Algorithms}
\begin{figure*}[ht]
  \centering
  \subfloat[Quicksort using MPI on 2 and 4 nodes for a range of input sizes]{
    \includegraphics{build/mpi}
    \label{fig:QMPI}
  }\quad\subfloat[PSRS using MPI on 2 and 4 nodes for a range of input sizes]{
    \includegraphics{build/rmpi}
    \label{fig:RMPI}
  }
  \caption{MPI implementations of quicksort and PSRS comparing relative speed-up
  when the number of nodes involved changes}
  \label{fig:MPI}
\end{figure*}

The MPI quicksort implementation followed much the same pattern as the OpenMP
implementation, however, it sends sub-sets of the list to the nodes
participating in the sort instead of pointers to the array in memory. This is
due to the nature of the message passing model where there is no shared memory.
The master node keeps any additional items in the list that remain after equally
dividing up the list between the nodes. This ensures that there no is
requirement on the size of the list, however, it does make the workload slightly
uneven.

Once each node has sorted their sub-set using a sequential quicksort, the
sub-sets are recombined. When recombining, the processors initially act as one
group, and the master of that group selects a pivot point, \(v\), that is the
mean of its sub-set. This is then broadcast to the other nodes and they find
where the index in their sub-sets such that \(X_{i + 1} > v\).

Next the nodes are divided into two groups, and the one sends the values lower
than the pivot to the other, while the other group sends the values higher than
the pivot to the first group. These are merged together using a mergesort and
the process is repeated in each group. Once there is only one node in each
group, the list is sorted and the \(p\) sub-sets are send to the master node.
The recombination process ensures that \(X_0\) of node \(p + 1\) is always
greater than \(X_n\) of node \(p\). Thus the master node simply places each
sub-set sequentially in the list without needing to use another mergesort.

The MPI implementation of PSRS follows the same pattern as the OpenMP
implementation, except that it has the added overhead of message-passing over a
network. It is based on the implementation by Krebs (2013)~\cite{krebs2013}.

Both implementations take the same number of comparisons as their shared-memory
counterparts, however they must also send sub-sets of the list between nodes.
This over the network communication incurs overhead that grows with the problem
size. The UCT HPC cluster uses 100Gb/s InfiniBand network connectivity. An
integer is 32 bits when compiled on the cluster. Thus, to send 1 million items
over the network, it will take
\[\frac{1}{100\times10^6}\times32\times10^6 = 0.32\text{ s}\]
The theoretical maximum number of items transmitted during the PSRS execution is
twice this~\cite{shi1992}.
Thus the total theoretical execution time for the PSRS MPI algorithm on 1
million items is
\[t_{rsmpi} = t_{rsomp} + 0.32 = 1.8\text{ s}\]

Thus the speed-up over the serial quicksort implementation is
\[S_{rsmpi} = \frac{t_s}{t_{rsmpi}} = 3.3\]

However, the quicksort algorithm requires each item to be sent over the network
multiple times. Assuming 4 nodes, each item is sent 4 times. Thus to send 1
million items will take 1.3 seconds. It follows tat the total theoretical
execution time for the quicksort MPI algorithm on 1 million items is
\[t_{qmpi} = t_{qomp} + 1.3 = 2.9\text{ s}\]

Thus the speed-up over the serial quicksort implementation is
\[S_{qmpi} = \frac{t_s}{t_{qmpi}} = 2.0\]

Validity of my implementation was tested through execution of tests with
varying input sizes. The input sizes were chosen to match closely with the input
sizes expected during the timing of the algorithm. These tests verified that the
final array was sorted by comparing each item to the one before it. The output
of the tests can be found in Appendix~\ref{app:TestOutput}.

\section{Benchmarking}
Benchmarking was performed to compare how well the two algorithms performed when
run in parallel. I expected to see speed-up in the ranges calculated in the
previous sections. Timing was done using the MPI and OpenMP wall-clock timers.
These timers provide a high-precision timer for calculating the overall time
that the algorithm spent sorting the list. I timed each algorithm 5 times, with
the average time being reported. This time excluded the generation of items for
each run. These 5 runs were each further run 10 times with each input size. This
was to ensure that the nominal case was considered that smoothed out any
outliers.

In order to calculate speed-up, the average time over the 10 repetitions for
each parallel algorithm implementation was compared to the average serial time
\[S_p(n_i) = \frac{\bar{t}_s(n_i)}{\bar{t}_p(n_i)}\]

For OpenMP, the number of threads was varied. Each 10 repetitions of each input
size was run using 2, 4 and 8 threads on 2, 4 and 8 cores. The MPI
implementations were tested on 2 and 4 nodes, each with a single core. As all
experiments were run on the UCT HPC cluster, all runs used 2.2GHz AMD Opteron
CPUs as the cores and a 100Gb/s InfiniBand inter-node network connection.

Figure~\ref{fig:OMP} describes the speed-up for the OpenMP implementations of
both quicksort and PSRS. The quicksort speed-up (Figure~\ref{fig:QOMP}) shows
clear, consistent speed-up that is in-line with the expected theoretical
speed-up of 4 times as fast for 4 threads. While the speed-up increases rapidly
initially, it levels off as the input size grows. However, the gain from adding
additional cores and threads grows. Thus, better performance could be achieved
by using additional cores and threads on a larger problem size, though this
would not scale infinitely due to limits on the physical size of hardware.

The performance of the PSRS OpenMP algorithm shown in Figure~\ref{fig:ROMP}
shows a slight improvement over the serial implementation for 1000 to 10000
items, but then the performance degrades for lists larger than that. I suspect
that this is due to the increased sizes of the local lists that are being sorted
locally combined with the synchronization overhead required for the PSRS
algorithm. There are also many memory allocation calls made by each thread that
have significant overhead associated. The 2 node, 2 core case could also not be
timed, as it took longer than I was allowed to run 10 repetitions for any input
size over 1000 items.

Figure~\ref{fig:MPI} describes the speed-up for the MPI implementations of the
two algorithms. The PSRS algorithm in Figure~\ref{fig:RMPI} shows a clear
speed-up when using 4 nodes, while the overhead when using 2 makes it slower
than the serial implementation. The quicksort implementation (Figure~\ref{QOMP})
shows slight speed-up for 2 nodes, but I was allocated an insufficient amount of
memory when using 4 nodes to test any input sizes larger than 524288 items,
though the graph can be extrapolated to assume that the performance would be
slower than the serial quicksort. This is likely due to the amount of data that
must be send over the network, combined with the many memory allocations
required.

From these results, it would be better to use OpenMP for a standard quicksort
and MPI for PSRS. For very large input sizes, MPI PSRS is set to perform and
scale better than OpenMP quicksort. It could also hold that combining the two,
using OpenMP to sort each node and MPI to handle multiple nodes, would improve
performance further for very large lists. However, this needs confirming through
empirical testing, as there could be excessive overhead incurred that does not
justify the use of OpenMP on each node.

\section{Conclusions}
This study has found that the highest speed-up was achieved using OpenMP to
implement a parallel quicksort. However, this does not scale infinitely and the
MPI PSRS implementation could outperform it on very large problem sizes. While
not tested, the results suggest that the best performance for large problem
sizes would be a combination of MPI PSRS and OpenMP quicksort. This could be
done using MPI PSRS to distribute and re-combine the entire list, while using
OpenMP on the individual nodes to sort the local sub-sets.

\section{Acknowledgements}
Computations were performed using facilities provided by the University of Cape
Town’s ICTS High Performance Computing team: \url{http://hpc.uct.ac.za}.

\bibliography{bibliography}{}
\bibliographystyle{ACM-Reference-Format}

\appendix
\section{Verification Output}
\label{app:TestOutput}
This is the output of the verification test that run through every sorting
algorithm three times with different output. The ``\texttt{vsetenv
PMIX\_SERVER\_TMPDIR failed}'' error reported does not affect the results, it is
an artefact of the HPC system I was using.

\begin{alltt}
\input{../output/test.out}
\end{alltt}
\end{document}
