###########################################################################
# AnalyzeData is Copyright (C) 2019 Kyle Robbertze <RBBKYL001@myuct.ac.za>
#
# AnalyzeData is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# AnalyzeData is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with AnalyzeData. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import sys
import math

import matplotlib.pyplot as plt

def parse_csv(filename):
    raw_results = []
    with open(filename) as f:
        for line in f:
            if line.startswith('#') or line.startswith('num'):
                continue
            values = line.split(',')
            nodes = int(values[0])
            cores = int(values[1])
            threads = int(values[2])
            input_size = int(values[3])
            time = float(values[4])
            result = {
                'nodes': nodes,
                'cores': cores,
                'threads': threads,
                'input_size': input_size,
                'time': time,
                'unique': True,
            }
            raw_results.append(result)
    filtered_results = []
    for i in range(0, len(raw_results)):
        if not raw_results[i]['unique']:
            continue
        result = raw_results[i]
        filtered_result = {
            'count': 1,
            'result': result,
        }
        for j in range(i, len(raw_results)):
            new_result = raw_results[j]
            if new_result['threads'] == result['threads'] and new_result['input_size'] == result['input_size']:
                filtered_result['count'] = filtered_result['count'] + 1
                filtered_result['result']['time'] = filtered_result['result']['time'] + new_result['time']
                raw_results[j]['unique'] = False

        filtered_result['result']['time'] = filtered_result['result']['time'] / filtered_result['count']
        filtered_results.append(filtered_result)

    return [r['result'] for r in filtered_results]

def render(x, y, label, line):
    fig_width_pt = 240.0
    inches_per_pt = 1.0 / 72.27
    golden_mean = (math.sqrt(5) - 1.0) / 2.0
    fig_width = fig_width_pt * inches_per_pt
    fig_height = fig_width * golden_mean
    fig_size = [fig_width, fig_height]
    params = {
        'backend': 'ps',
        'axes.labelsize': 9,
        'font.size': 9,
        'figure.titlesize': 9,
        'axes.titlesize': 9,
        'legend.fontsize': 9,
        'xtick.labelsize': 8,
        'ytick.labelsize': 8,
        'text.usetex': True,
        'figure.figsize': fig_size,
    }
    plt.rcParams.update(params)
    plt.plot(x, y, '{}'.format(line), label=label)
    plt.axis([0, 1005000, 0, 6.5])
    plt.xlabel('Input size')
    plt.ylabel('Speed-up')
    plt.legend();

def get_results(filename):
    results = parse_csv(filename)
    if len(results) % 5 != 0:
        print('File {filename} is missing some tests!'.format(filename=filename), file=sys.stderr)
        for result in results:
            print(result, file=sys.stderr)
    return results

def get_speedup(new, old):
    return [old[i]['time'] / new[i]['time'] for i in range(0, len(old))]

if __name__ == '__main__':
    serial = get_results('../output/csv/serial.csv')
    serial_mpi4 = get_results('../output/csv/serial_mpi4.csv')
    mpi2 = get_results('../output/csv/mpi/mpi2.csv')
    mpi4 = get_results('../output/csv/mpi/mpi4.csv')

    omp2 = get_results('../output/csv/omp/omp2.csv')
    omp4 = get_results('../output/csv/omp/omp4.csv')
    omp8 = get_results('../output/csv/omp/omp8.csv')

    romp4 = get_results('../output/csv/romp/romp4.csv')
    romp8 = get_results('../output/csv/romp/romp8.csv')

    rmpi2 = get_results('../output/csv/rmpi/rmpi2.csv')
    rmpi4 = get_results('../output/csv/rmpi/rmpi4.csv')

    speedup_omp2_2 = get_speedup([r for r in omp2 if r['threads'] == 2], serial)
    speedup_omp4_4 = get_speedup([r for r in omp4 if r['threads'] == 4], serial)
    speedup_omp8_8 = get_speedup([r for r in omp8 if r['threads'] == 8], serial)

    render([r['input_size'] for r in serial], speedup_omp2_2, '2 threads', '-')
    render([r['input_size'] for r in serial], speedup_omp4_4, '4 threads', '-.')
    render([r['input_size'] for r in serial], speedup_omp8_8, '8 threads', ':')
    plt.title('OpenMP quicksort speed-up')
    plt.savefig('graphics/omp.eps')
    plt.clf()

    speedup_mpi2 = get_speedup(mpi2, serial)
    speedup_mpi4 = get_speedup(mpi4, serial_mpi4)

    render([r['input_size'] for r in serial], speedup_mpi2, '2 nodes', '-')
    render([r['input_size'] for r in serial_mpi4], speedup_mpi4, '4 nodes', '-.')
    plt.title('MPI quicksort speed-up')
    plt.savefig('graphics/mpi.eps')
    plt.clf()

    speedup_romp4_4 = get_speedup([r for r in romp4 if r['threads'] == 4], serial)
    speedup_romp8_8 = get_speedup([r for r in romp8 if r['threads'] == 8], serial)

    render([r['input_size'] for r in serial], speedup_romp4_4, '4 threads', '-')
    render([r['input_size'] for r in serial], speedup_romp8_8, '8 threads', '-.')
    plt.title('OpenMP PSRS speed-up')
    plt.savefig('graphics/romp.eps')
    plt.clf()

    speedup_rmpi2 = get_speedup(rmpi2, serial)
    speedup_rmpi4 = get_speedup(rmpi4, serial)

    render([r['input_size'] for r in serial], speedup_rmpi2, '2 nodes', '-')
    render([r['input_size'] for r in serial], speedup_rmpi4, '4 nodes', '-.')
    plt.title('MPI PSRS speed-up')
    plt.savefig('graphics/rmpi.eps')
    plt.clf()
