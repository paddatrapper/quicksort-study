/*******************************************************************************
 * Copyright (c) 2019, Kyle Robbertze
 *
 * Adapted from the pseudocode provided in Cormen, Thomas H., et al.
 * Introduction to algorithms. MIT press, 2009.
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  A serial quicksort implementation
 *
 *        Created:  09/05/2019 19:10:18
 *       Compiler:  gcc
 *
 *         Author:  Kyle Robbertze (kr), RBBKYL001@myuct.ac.za
 ******************************************************************************/

#include "quicksort.h"

void swap(int array[], int left, int right) {
    int temp;
    temp = array[left];
    array[left] = array[right];
    array[right] = temp;
}

int partition(int array[], int left, int right, int pivot_index) {
    int pivot_value = array[pivot_index];
    int store_index = left;
    swap(array, pivot_index, right);
    for (int i = left; i < right; ++i) {
        if (array[i] <= pivot_value) {
            swap(array, i, store_index);
            ++store_index;
        }
    }
    swap(array, store_index, right);
    return store_index;
}

void quicksort_serial_run(int array[], int left, int right) {
    if (left < right) {
        int pivot_index = partition(array, left, right, left); 
        quicksort_serial_run(array, left, pivot_index - 1);
        quicksort_serial_run(array, pivot_index + 1, right);
    }
}

void quicksort_serial(int array[], int length) {
    quicksort_serial_run(array, 0, length - 1);
}
