/*******************************************************************************
 * Copyright (c) 2019, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Quicksort algorithms using serial and parallel
 *                  implementations.
 *
 *        Created:  09/05/2019 19:10:59
 *       Compiler:  gcc
 *
 *         Author:  Kyle Robbertze (kr), RBBKYL001@myuct.ac.za
 ******************************************************************************/

#ifndef QUICKSORT_H
#define QUICKSORT_H

#define OMP_CUTOFF 1000
#define MPI_ROOT_RESULT 666
#define MPI_GROUP_EXCHANGE 667

void swap(int array[], int left, int right);
int partition(int array[], int left, int right, int pivot_index);
void quicksort_serial(int array[], int length);
void merge(int source1[], int source1_length, int source2[],
        int source2_length, int dest[]);
void quicksort_parallel_omp(int array[], int length);
void quicksort_parallel_mpi(int array[], int length);
#endif
