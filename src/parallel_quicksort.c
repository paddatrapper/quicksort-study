/*******************************************************************************
 * Copyright (c) 2019, Kyle Robbertze
 *
 * MPI Quicksort implementation based on https://github.com/Row/MPIQuicksort
 * and https://cse.buffalo.edu/faculty/miller/Courses/CSE633/Ramkumar-Spring-2014-CSE633.pdf
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Parallel implementations of quicksort using OpenMP
 *                  and OpenMPI
 *
 *        Created:  13/05/2019 10:07:13
 *       Compiler:  gcc
 *
 *         Author:  Kyle Robbertze (kr), RBBKYL001@myuct.ac.za
 ******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#include "quicksort.h"

void quicksort_parallel_omp_run(int array[], int left, int right) {
    int pivot_index = partition(array, left, right, left); 
    if (left < right && right - left < OMP_CUTOFF) {
        quicksort_parallel_omp_run(array, left, pivot_index - 1);
        quicksort_parallel_omp_run(array, pivot_index + 1, right);

    } else if (right - left > OMP_CUTOFF) {
#pragma omp task
        quicksort_parallel_omp_run(array, left, pivot_index - 1);
#pragma omp task
        quicksort_parallel_omp_run(array, pivot_index + 1, right);
    }
}

void quicksort_parallel_omp(int array[], int length) {
#pragma omp parallel
    {
#pragma omp single nowait
        quicksort_parallel_omp_run(array, 0, length - 1);
    }
}

void merge(int source1[], int source1_length, int source2[], int source2_length,
        int dest[]) {
    int i = 0;
    int j = 0;
    int k = 0;
    while (i < source1_length && j < source2_length) {
        if (source1[i] <= source2[j]) {
            dest[k] = source1[i];
            ++i;
        } else {
            dest[k] = source2[j];
            ++j;
        }
        ++k;
    }
    if (i < source1_length) {
        for (int p = i; p < source1_length; ++p) {
            dest[k] = source1[p];
            ++k;
        }
    } else {
        for (int p = j; p < source2_length; ++p) {
            dest[k] = source2[p];
            ++k;
        }

    }
}

int mpi_recombine(int* array[], int length, MPI_Comm comm_group) {
    int nproc, rank;

    MPI_Comm_size(comm_group, &nproc);
    MPI_Comm_rank(comm_group, &rank);

    // Now has unique set of numbers
    if (nproc < 2) {
        return length;
    }
    int* local_array = *array;

    int pivot_value = (local_array[0] + local_array[length - 1]) / 2;
    MPI_Bcast(&pivot_value, 1, MPI_INT, 0, comm_group);

    int pivot_index = length / 2;
    while (pivot_index < length && local_array[pivot_index] <= pivot_value)
        pivot_index++;
    while (pivot_index > 0 && local_array[pivot_index - 1] > pivot_value)
        pivot_index--;

    int group = rank / (nproc / 2);
    int dest, offset, chunk;

    if (group) {
        dest = rank - nproc / 2;
        offset = 0;
        chunk = pivot_index;
    } else {
        dest = rank + nproc / 2;
        offset = pivot_index;
        chunk = length - pivot_index;
    }

    MPI_Request request;
    MPI_Status status;
    int recv_length;
    MPI_Isend(&local_array[offset], chunk, MPI_INT, dest, MPI_GROUP_EXCHANGE,
           comm_group, &request);
    MPI_Probe(dest, MPI_GROUP_EXCHANGE, comm_group, &status);
    MPI_Get_count(&status, MPI_INT, &recv_length);

    int* recv_array = (int*) malloc(recv_length * sizeof(int));
    MPI_Recv(recv_array, recv_length, MPI_INT, dest, MPI_GROUP_EXCHANGE, 
            comm_group, &status);

    int new_length = recv_length + length - chunk;
    offset = offset ? 0 : pivot_index;
    int* new_array = (int*) malloc(new_length * sizeof(int));

    merge(&local_array[offset], length - chunk, recv_array, recv_length,
            new_array);

    MPI_Wait(&request, &status);

    free(local_array);
    free(recv_array);
    *array = new_array;

    MPI_Comm group_comm;
    MPI_Comm_split(comm_group, group, rank, &group_comm);

    return mpi_recombine(array, new_length, group_comm);
}

void quicksort_parallel_mpi(int array[], int length) {
    int nproc, rank;

    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int chunk = length / nproc;
    int other_chunks = chunk;
    int *send_countv = (int*)malloc(nproc * sizeof(int));
    if (rank == 0) {
        // Root node takes additional elements if length is not divisible by nproc
        chunk += length % nproc;
    }
    int* local_array = (int*)malloc(chunk * sizeof(int));
    int* displs = (int*)malloc(nproc * sizeof(int));

    int offset = 0;
    for (int i = 0; i < nproc; ++i) {
        displs[i] = offset;
        send_countv[i] = (i == 0) ? chunk : other_chunks;
        offset += send_countv[i];
    }

    MPI_Scatterv(array, send_countv, displs, MPI_INT, local_array, chunk,
            MPI_INT, 0, MPI_COMM_WORLD);
    free(send_countv);
    free(displs);

    quicksort_serial(local_array, chunk);
    
    chunk = mpi_recombine(&local_array, chunk, MPI_COMM_WORLD);

    MPI_Request request;
    MPI_Isend(local_array, chunk, MPI_INT, 0, MPI_ROOT_RESULT, MPI_COMM_WORLD,
            &request);

    if (rank == 0) {
        int offset = 0;
        MPI_Status status;
        int recv_length;
        for (int i = 0; i < nproc; ++i) {
            MPI_Recv(&array[offset], length, MPI_INT, i,
                    MPI_ROOT_RESULT, MPI_COMM_WORLD, &status);
            MPI_Get_count(&status, MPI_INT, &recv_length);
            offset += recv_length;
        }
    }
    MPI_Status status;
    MPI_Wait(&request, &status);

    free(local_array);
}
