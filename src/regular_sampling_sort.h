/*******************************************************************************
 * Copyright (c) 2019, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Regular sampling sorting algorithms for OpenMP and MPI
 *
 *        Created:  29/05/2019 20:53:33
 *       Compiler:  gcc
 *
 *         Author:  Kyle Robbertze (kr), RBBKYL001@myuct.ac.za
 ******************************************************************************/

#ifndef REGULAR_SAMPLING_SORT_H
#define REGULAR_SAMPLING_SORT_H

#define MASTER if (rank == 0)

typedef struct {
	int size;
	int *arr;
} intArray;

void regular_sampling_sort_omp(int array[], int length);
void regular_sampling_sort_mpi(int array[], int length);
#endif
