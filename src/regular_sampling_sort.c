/*******************************************************************************
 * Copyright (c) 2019, Kyle Robbertze
 *
 * OMP implementation based on
 * https://github.com/Malhadas/Study-on-Parallel-Sorting-by-Regular-Sampling
 *
 * MPI implementation based on
 * https://github.com/a-krebs/psrs
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Implementations of Regular Sampling sort for
 *                  OpenMP and MPI
 *
 *        Created:  29/05/2019 21:03:52
 *       Compiler:  gcc
 *
 *         Author:  Kyle Robbertze (kr), RBBKYL001@myuct.ac.za
 ******************************************************************************/
#include <mpi.h>
#include <omp.h>
#include <stdlib.h>
#include <memory.h>

#include "regular_sampling_sort.h"
#include "quicksort.h"

void merge_sort(int array[], int length) {
    if (length > 1) {
        int middle = length / 2;
        int* left = (int*) malloc(middle * sizeof(int));
        memcpy(left, array, middle * sizeof(int));
        int* right = array + middle;
        merge_sort(left, middle);
        merge_sort(right, length - middle);
        merge(left, middle, right, length - middle, array);
    }
}

void calc_partition_borders(int array[], int start, int end, int result[],
        int at, int pivots[], int first_pv, int last_pv) {
    int mid = (first_pv + last_pv) / 2;
    int pv = pivots[mid-1];
    int lowerbound = start;
    int upperbound = end;

    while(lowerbound <= upperbound) {
        int center = (lowerbound + upperbound) / 2;
        if(array[center] > pv) {
            upperbound = center - 1;
        } 
        else {
            lowerbound = center + 1;
        }
    }

    result[at + mid] = lowerbound;

    if(first_pv < mid) {
        calc_partition_borders(array, start, lowerbound - 1, result, at,
                pivots, first_pv, mid - 1);
    }

    if(mid < last_pv) {
        calc_partition_borders(array, lowerbound, end, result, at, pivots,
                mid + 1, last_pv);
    }
}

void regular_sampling_sort_omp(int array[], int length) {
    int num_threads = omp_get_max_threads();
    int size = (length + num_threads - 1) / num_threads;
    int rsize = (size + num_threads - 1) / num_threads;
    int sample_length = num_threads * (num_threads - 1);

    int** loc_a_ptrs = (int**) malloc(num_threads * sizeof(int*));
    int* samples = (int*) malloc(sample_length * sizeof(int));
    int* partition_borders =
        (int*) malloc(num_threads * (num_threads + 1) * sizeof(int));
    int* bucket_sizes = (int*) malloc(num_threads * sizeof(int));
    int* result_positions = (int*) malloc(num_threads * sizeof(int));
    int* pivots = (int*) malloc((num_threads - 1) * sizeof(int));

    // The array is only read from and so is free from concurrency issues
#pragma omp parallel shared(array, loc_a_ptrs, samples, partition_borders, bucket_sizes, result_positions, pivots, num_threads, size)
    {
        int thread_num = omp_get_thread_num();
        int start = thread_num * size;
        int end = start + size - 1;

        if (end >= length) end = length - 1;
        int local_length = (end - start + 1);
        end = end % size;
        int* local_array = (int*) malloc(local_length * sizeof(int));
        memcpy(local_array, array + start, local_length * sizeof(int));
        // Write to a unique location in the array
        loc_a_ptrs[thread_num] = local_array;
        quicksort_serial(local_array, local_length);
        int offset = thread_num * (num_threads - 1) - 1;
        for (int i = 0; i < num_threads; ++i) {
            if (i * rsize <= end) {
                samples[offset + i] = local_array[i * rsize - 1];
            } else {
                samples[offset + i] = local_array[end];
            }
        }
#pragma omp barrier
#pragma omp single
        {
            quicksort_serial(samples, sample_length);
            for (int i = 0; i < num_threads - 1; ++i)
                pivots[i] = samples[i * num_threads + num_threads / 2];
        }
#pragma omp barrier
        offset = thread_num * (num_threads + 1);
        partition_borders[offset] = 0;
        partition_borders[offset + num_threads] = end + 1;
        calc_partition_borders(local_array, 0, local_length - 1,
                partition_borders, offset, pivots, 1, num_threads - 1);
#pragma omp barrier

        int max = num_threads * (num_threads + 1);
        bucket_sizes[thread_num] = 0;
        for (int i = thread_num; i < max; i += num_threads + 1) {
            bucket_sizes[thread_num] +=
                partition_borders[i + 1] - partition_borders[i];
        }
#pragma omp barrier
#pragma omp single
        {
            result_positions[0] = 0;
            for(int i = 1; i < num_threads; i++) {
                result_positions[i] = bucket_sizes[i-1] + 
                    result_positions[i-1];
            }
        }
#pragma omp barrier
        int* this_result = array + result_positions[thread_num];
        int this_result_length = (thread_num == num_threads - 1) ?
            length - result_positions[thread_num] :
            result_positions[thread_num + 1] - result_positions[thread_num];
        for (int i = 0, j = 0; i < num_threads; i++) {
            offset = i * (num_threads + 1) + thread_num;
            int low = partition_borders[offset];
            int high = partition_borders[offset+1];
            int partition_size = (high - low);

            if (partition_size > 0) {
                memcpy(this_result+j, &(loc_a_ptrs[i][low]),
                        partition_size * sizeof(int));
                j += partition_size;
            }
        }
        quicksort_serial(this_result, this_result_length);

#pragma omp barrier
        free(local_array);
    }

    free(loc_a_ptrs);
    free(samples);
    free(partition_borders);
    free(bucket_sizes);
    free(result_positions);
    free(pivots);
}


/*
 * Partition data into p parts and scatter parts to other processes
 * int the communicator.
 */
void scatter_data(int *data, int *local, int count) {
    MPI_Scatter(
            data,
            count,
            MPI_INT,
            local,
            count,
            MPI_INT,
            0,
            MPI_COMM_WORLD);
}

/*
 * Phase 1
 *
 * Each processor is assigned n/p items.
 * Each processor sorts their portion of the items using quicksort.
 * Each processor takes regular samples of their sorted local data.
 */
void phase_1(
        int rank, intArray *data, intArray *local,
        intArray *samples, int interval) {

    int i = 0;

    /* sort local data */
    // TODO make sure we're actually using quicksort
    quicksort_serial(local->arr, local->size);

    /* take samples */
    for (i = 0; i < local->size; i += interval) {
        samples->arr[i/interval] = local->arr[i];
    }
}

/*
 * Gather samples from all processors to MASTER
 */
void gather_samples(
        int rank, int size, intArray* gatheredSamples, intArray *samples) {
    MASTER {
        gatheredSamples->size = size * size;
        gatheredSamples->arr = calloc(
                gatheredSamples->size, sizeof(int));
    }

    /* gather samples on MASTER */
    MPI_Gather(
            samples->arr,
            samples->size,
            MPI_INT,
            gatheredSamples->arr,
            /* recvcount is size sent from a single process */
            samples->size,
            MPI_INT,
            0,
            MPI_COMM_WORLD);

    return;
}

/*
 * Use gathered samples to select pivots
 * and broadcase them to other processes
 */
void broadcast_pivots(
        int rank, int size, intArray *gatheredSamples, intArray *pivots) {
    int i = 0;
    /* interval at which to select pivots
     * (implicit floor because these are integers) */
    int k = size / 2;

    MASTER {
        /* sort samples */
        quicksort_serial(gatheredSamples->arr, gatheredSamples->size);

        /* select p-1 pivots */
        for (i = 0; i < pivots->size; i++) {
            pivots->arr[i] = gatheredSamples->arr[((i+1)*size) + k];
        }
    }

    /* broadcast pivots */
    MPI_Bcast(
            pivots->arr,
            pivots->size,
            MPI_INT,
            0,
            MPI_COMM_WORLD);
}

/*
 * Re-assign the arr pointers for the first numP partitions in partitions array.
 */
void reset_arr_pointers(
        intArray *partitionsHead, intArray **partitions, int numP) {

    int i = 0;
    int *currArr = partitionsHead->arr;

    for (i = 0; i < numP; i++) {
        partitions[i]->arr = currArr;
        currArr += partitions[i]->size;
    }

    return;
}



/*
 * Partition local data based on pivots
 */
void partition_data(
        int rank, int size, intArray *local, intArray *pivots,
        intArray **partitions, intArray *partitionsHead) {

    int i = 0;
    int j = 0;
    int k = 0;
    int pivot = 0;
    int index = 0;
    int partitionSize = 0;
    int repeatedPivot = 0;

    /* there are size-1 pivots for size number of partitions */
    for (i = 0; i < pivots->size; i++) {
        pivot = pivots->arr[i];
        repeatedPivot = 0;

        for (j = index; j < local->size; j++) {
            if (local->arr[j] < pivot) {
                partitionSize++;
            } else {
                /* include pivot if it repeats*/
                for (k = j + 1; k < local->size; k++) {
                    if (local->arr[k] == pivot) {
                        repeatedPivot++;
                    } else {
                        break;
                    }
                }
                partitionSize += repeatedPivot;
                j += repeatedPivot;
                break;
            }
        }

        /* allocate for partition */
        partitions[i] = calloc(1, sizeof(intArray));
        partitions[i]->size = partitionSize;
        partitionsHead->size += partitions[i]->size;
        partitionsHead->arr = realloc(partitionsHead->arr, partitionsHead->size * sizeof(int));
        reset_arr_pointers(partitionsHead, partitions, i + 1);
        memset(partitions[i]->arr, 0, partitions[i]->size * sizeof(int));

        /* copy values to partition */
        while (partitionSize > 0) {
            partitions[i]->arr[partitionSize - 1] = local->arr[partitionSize + index - 1];
            partitionSize--;
        }

        /* set start index for next partition */
        index = j;
        // skip pivot
        if (local->arr[index] == pivot) {
            index++;
        }
    }

    /* add remaining values to final parition */
    /* using i from loop above */
    partitions[i] = calloc(1, sizeof(intArray));
    partitions[i]->size = local->size - index;

    partitionsHead->size += partitions[i]->size;
    partitionsHead->arr = realloc(partitionsHead->arr, partitionsHead->size * sizeof(int));
    partitions[i]->arr = partitionsHead->arr + partitionsHead->size - partitions[i]->size;
    memset(partitions[i]->arr, 0, partitions[i]->size * sizeof(int));

    for (j = index; j < local->size; j++) {
        // add to final partition
        partitions[i]->arr[j - index] = local->arr[j];
    }
}

/*
 * Phase 2
 *
 * MASTER processor gathers and sorts the regular samples.
 * p-1 pivots are selected from the regular samples at indeces
 *     p+k, 2p+k, 3p+k, ... , (p-1)+k
 *     where k = floor(p/2)
 * Each processor receives a copy of the pivots
 * Each processor makes p partitions from their local data
 */
void phase_2(
        int rank, int size, intArray *samples, intArray *local, intArray *pivots,
        intArray **partitions, intArray *partitionsHead) {

    /* loop variables */
    intArray *gatheredSamples = NULL;

    /* gather samples on MASTER */
    gatheredSamples = calloc(1, sizeof(intArray));
    gather_samples(rank, size, gatheredSamples, samples);

    /* select and broadcase pivots */
    broadcast_pivots(rank, size, gatheredSamples, pivots);

    /* done with gatheredSamples */
    free(gatheredSamples->arr);
    free(gatheredSamples);

    /* partition local data based on pivots */
    partition_data(rank, size, local, pivots, partitions, partitionsHead);

    return;
}

/*
 * echange parition sizes with other processors
 */
void exchange_partition_sizes(
        int rank, intArray *partSizes, intArray *newPartSizes) {

    /* exchange partition sizes with all others */
    MPI_Alltoall(
            partSizes->arr,
            1,
            MPI_INT,
            newPartSizes->arr,
            1,
            MPI_INT,
            MPI_COMM_WORLD);
}

/*
 * Exchange partitions with other processes
 */
void exchange_partitions(
        int rank, int size, 
        intArray *partSizes, intArray *newPartSizes,
        intArray **partitions, intArray *partitionsHead,
        intArray **newPartitions, intArray *newPartitionsHead) {

    int i = 0;
    int offset = 0;
    int totalPartSize = 0;
    int *rdispls = NULL;
    int *sdispls = NULL;

    /* buffers for MPI_Alltoallv call */
    sdispls = calloc(size, sizeof(int));
    rdispls = calloc(size, sizeof(int));

    /* get partition offset in local partitionsHead array */
    offset = 0;
    for (i = 0; i < size; i++) {
        sdispls[i] = offset;
        offset += partSizes->arr[i];
    }

    /* calculate total memory needed for incoming partitions*/
    for (i = 0; i < newPartSizes->size; i++) {
        totalPartSize += newPartSizes->arr[i];
    }

    /* allocate memory for each parition */
    newPartitionsHead->arr = calloc(totalPartSize, sizeof(int));
    offset = 0;
    for (i = 0; i < newPartSizes->size; i++) {
        newPartitions[i] = calloc(1, sizeof(intArray));
        newPartitions[i]->size = newPartSizes->arr[i];
        newPartitions[i]->arr = newPartitionsHead->arr + offset;
        /* while we're looping, keep track of
         * partition offset in newPartitionsHead */
        rdispls[i] = offset;
        offset += newPartSizes->arr[i];
    }

    /* exchange partitions */
    MPI_Alltoallv(
            partitionsHead->arr,
            partSizes->arr,
            sdispls,
            MPI_INT,
            newPartitionsHead->arr,
            newPartSizes->arr,
            rdispls,
            MPI_INT,
            MPI_COMM_WORLD);

    free(sdispls);
    free(rdispls);
}

/*
 * Phase 3
 *
 * Each porcessor i keeps the ith partition and sends the jth partition to the
 * jth processor
 */
void phase_3(
        int rank, int size, intArray ***partitionsPtr,
        intArray **partitionsHeadPtr) {

    int i = 0;
    intArray **partitions = NULL;
    intArray *partitionsHead = NULL;

    intArray *partSizes = NULL;
    intArray *newPartSizes = NULL;
    intArray **newPartitions = NULL;
    intArray *newPartitionsHead = NULL;

    partitions = *partitionsPtr;
    partitionsHead = *partitionsHeadPtr;

    /* buffer to exchange partition sizes */
    partSizes = calloc(1, sizeof(intArray));
    partSizes->size = size;
    partSizes->arr = calloc(partSizes->size, sizeof(int));
    newPartSizes = calloc(1, sizeof(intArray));
    newPartSizes->size = size;
    newPartSizes->arr = calloc(newPartSizes->size, sizeof(int));

    /* new partitions (from other processors) */
    newPartitions = calloc(size, sizeof(intArray*));
    newPartitionsHead = calloc(1, sizeof(intArray));

    /* set local sizes */
    for (i = 0; i < size; i++) {
        partSizes->arr[i] = partitions[i]->size;
    }

    /* swap partition sizes */
    exchange_partition_sizes(rank, partSizes, newPartSizes);

    /* swap partitions */
    exchange_partitions(
            rank, size,
            partSizes, newPartSizes,
            partitions, partitionsHead,
            newPartitions, newPartitionsHead);

    /* swap old local paritions with new ones */
    free(partitionsHead->arr);
    free(partitionsHead);
    for (i = 0; i < size; i++) {
        free(partitions[i]);
    }
    free(partitions);
    *(partitionsHeadPtr) = newPartitionsHead;
    *(partitionsPtr) = newPartitions;

    /* clean up */
    free(partSizes->arr);
    free(partSizes);
    free(newPartSizes->arr);
    free(newPartSizes);
    return;
}

/*
 * Merge the local partitions
 */
void phase_4(int rank, int size, intArray **partitions, intArray *finalList) {

    int i = 0;
    int j = 0;
    int partWithMin = 0;
    int minVal = 0;
    int *indices = NULL;

    for (i = 0; i < size; i++) {
        finalList->size += partitions[i]->size;
    }

    finalList->arr = calloc(finalList->size, sizeof(int));
    indices = calloc(size, sizeof(int));

    /* do a k-way merge of already sorted lists */
    for (i = 0; i < finalList->size; i++) {
        for (j = 0; j < size; j++) {
            if (indices[j] < partitions[j]->size) {
                partWithMin = j;
                minVal = partitions[j]->arr[indices[j]];
            }
        }

        for (j = 0; j < size; j++) {
            if (indices[j] >= partitions[j]->size) {
                continue;
            }

            if (partitions[j]->arr[indices[j]] < minVal) {
                minVal = partitions[j]->arr[indices[j]];
                partWithMin = j;
            }
        }

        finalList->arr[i] = minVal;
        indices[partWithMin]++;
    }
}

/*
 * Phase 5 (not actually a phase in PSRS)
 *
 * Gather final lists from all processes
 * Concatenate final lists and pivots
 */
void phase_5(int rank, int size, int nElem, intArray *finalList,
        intArray *pivots, int array[]) {

    int i = 0;
    int offset = 0;
    int sizes[size];
    int displs[size];

    memset(sizes, 0, size * sizeof(int));
    memset(displs, 0, size * sizeof(int));

    /* exchange finalList sizes with all processors */
    /* gather sizes at MASTER */
    MPI_Gather(&(finalList->size), 1, MPI_INT, sizes, 1, MPI_INT, 0,
            MPI_COMM_WORLD);

    /* figure out displs for recieve */
    MASTER {
        offset = 0;
        for (i = 0; i < size; i++) {
            displs[i] = offset;
            /* +1 to add spaces between lists for pivots */
            offset += sizes[i] + 1;
        }
    }

    /* gather final lists on MASTER */
    MPI_Gatherv(
            finalList->arr,
            finalList->size,
            MPI_INT,
            array,
            sizes,
            displs,
            MPI_INT,
            0,
            MPI_COMM_WORLD);

    MASTER {
        /* insert pivots into concatList */
        offset = 0;
        for (i = 0; i < pivots->size; i++) {
            offset += sizes[i];
            array[offset] = pivots->arr[i];
            offset++;
        }
    }
}

void regular_sampling_sort_mpi(int array[], int length) {
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    /* loop variables */
    int i;
    /* complete random dataset, only filled on MASTER */
    intArray *data = NULL;
    /* local data, samples, pivots*/
    intArray *local = NULL;
    intArray *samples = NULL;
    intArray *pivots = NULL;
    /* partitions and final sorted list */
    /* 
     * partitions are in a contguous memory area so that the entire memory
     * area can be passed into MPI_Alltoallv. The individual partition
     * structs in the **partitions array each have their arr pointer point
     * to a space within partitionsHead->arr. Free only the
     * partitionsHead-arr when done (and each of the partitions[i] structs,
     * but NOT partitions[i]>arr).
     */
    intArray **partitions = NULL;
    intArray *partitionsHead = NULL;
    intArray *finalList = NULL;

    /* what is the interval at which to take samples? */
    int interval = length / (size * size);
    if (interval == 0) {
        interval = 1;
    }

    /* initialize local buffers */
    local = calloc(1, sizeof(intArray));
    local->size = length / size;
    local->arr = calloc(local->size, sizeof(int));
    samples = calloc(1, sizeof(intArray));
    /* each processor takes p samples */
    samples->size = size;
    samples->arr = calloc(samples->size, sizeof(int));
    pivots = calloc(1, sizeof(intArray));
    /* select p-1 privots from samples */
    pivots->size = size - 1;
    pivots->arr = calloc(pivots->size, sizeof(int));
    partitions = calloc(size, sizeof(intArray *));
    partitionsHead = calloc(1, sizeof(intArray));
    finalList = calloc(1, sizeof(intArray));

    /* generate list of integers */
    /* all procs need access to data->arr even it is is NULL */
    data = calloc(1, sizeof(intArray));
    MASTER {
        data->size = length;
        data->arr = array;
    }

    /* split data into p partitions and scatter to other pocesses */
    scatter_data(data->arr, local->arr, local->size);

    /* Phase 1: partition and sort local data */
    phase_1(rank, data, local, samples, interval);

    /* Phase 2: find pivots then partition */
    phase_2(rank, size, samples, local, pivots, partitions, partitionsHead);

    /* Phase 3: exchange partitions */
    phase_3(rank, size, &partitions, &partitionsHead);

    /* Phase 4: merge partitions */
    phase_4(rank, size, partitions, finalList);

    /* gather data and print for verification */
    phase_5(rank, size, length, finalList, pivots, array);

    free(data);
    free(local->arr);
    free(local);
    free(samples->arr);
    free(samples);
    free(pivots->arr);
    free(pivots);
    free(partitionsHead->arr);
    free(partitionsHead);
    for (i = 0; i < size; i++) {
        free(partitions[i]);
    }
    free(partitions);
    free(finalList->arr);
    free(finalList);
}
