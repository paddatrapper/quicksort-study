/*******************************************************************************
 * Copyright (c) 2019, Kyle Robbertze
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * It is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this project. If not, see <http://www.gnu.org/licenses/>.
 *
 *    Description:  Main entry point
 *
 *        Created:  10/05/2019 07:58:48
 *       Compiler:  gcc
 *
 *         Author:  Kyle Robbertze (kr), RBBKYL001@myuct.ac.za
 *******************************************************************************/
#include <argp.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <omp.h>
#include <mpi.h>

#include "quicksort.h"
#include "regular_sampling_sort.h"

#define RUNS 5

const char* argp_program_version = "version 1.0-dev";

struct flags {
    int array_size;
    int seed;
    int cores;
    bool test;
    bool use_mpi;
    void (*sort)(int*, int);
    double (*timer)();
    char* algorithm;
};

/**
 * Prints an array to the standard output
 */
void display(int array[], int length) {
    for (int i = 0; i < length; ++i) {
        printf("%d ", array[i]);
    }
    printf("\n");
}

/**
 * Determins the sorting algorithm selected by the user.
 * Valid options are "serial", "omp" and "mpi"
 */
int get_sort_algorithm(char arg[], void (**sort)(int*, int), double (**timer)(), bool* use_mpi) {
    if (strcmp("serial", arg) == 0) {
        *sort = quicksort_serial;
        *timer = omp_get_wtime;
        *use_mpi = false;
        return 0;
    } else if (strcmp("qomp", arg) == 0) {
        *sort = quicksort_parallel_omp;
        *timer = omp_get_wtime;
        *use_mpi = false;
        return 0;
    } else if (strcmp("qmpi", arg) == 0) {
        *sort = quicksort_parallel_mpi;
        *timer = MPI_Wtime;
        *use_mpi = true;
        return 0;
    } else if (strcmp("regomp", arg) == 0) {
        *sort = regular_sampling_sort_omp;
        *timer = omp_get_wtime;
        *use_mpi = false;
        return 0;
    } else if (strcmp("regmpi", arg) == 0) {
        *sort = regular_sampling_sort_mpi;
        *timer = MPI_Wtime;
        *use_mpi = true;
        return 0;
    }
    return 1;
}

/**
 * Parses the arguments passed to the application and sets the indicating
 * flags to modify the application behaviour.
 */
static int parse_opt(int key, char arg[], struct argp_state* state) {
    struct flags* a = state->input;
    switch (key) {
        case 'n': {
            int size = atoi(arg);
            if (size < 0)
                argp_failure(state, 1, 0, "size should be positive");
            a->array_size = size;
            break;
        } case 's': {
            int seed = atoi(arg);
            if (seed < 0)
                argp_failure(state, 1, 0, "seed should be positive");
            a->seed = seed;
            break;
        } case 't':
            a->test = true;
            break;
        case 'p': {
            int result = get_sort_algorithm(arg, &a->sort, &a->timer, &a->use_mpi);
            if (result)
                argp_failure(state, result, 0, "unknown sorting algorithm");
            a->algorithm = arg;
            break;
        } case 'c': {
            int cores = atoi(arg);
            if (cores < 0)
                argp_failure(state, 1, 0, "number of cores should be positive");
            a->cores = cores;
            break;
        }
        case ARGP_KEY_INIT:
            a->array_size = 10;
            a->seed = time(NULL);
            a->test = false;
            a->sort = quicksort_serial;
            a->timer = omp_get_wtime;
            a->use_mpi = false;
            a->algorithm = "serial";
            a->cores = 0;
            break;
    }
    return 0;
}

/**
 * Tests the algorithms to ensure that they all sort correctly
 */
int test(int* array, int length) {
    for (int i = 0; i < length; ++i)
        array[i] = rand();
    int* omp_array = (int*) malloc(length * sizeof(int));
    memcpy(omp_array, array, length * sizeof(int));
    int* mpi_array = (int*) malloc(length * sizeof(int));
    memcpy(mpi_array, array, length * sizeof(int));
    int* regomp_array = (int*) malloc(length * sizeof(int));
    memcpy(regomp_array, array, length * sizeof(int));
    int* regmpi_array = (int*) malloc(length * sizeof(int));
    memcpy(regmpi_array, array, length * sizeof(int));
    int* serial_array = array;

    printf("Running serial quicksort...\n");
    quicksort_serial(serial_array, length);
    printf("Running OpenMP quicksort...\n");
    quicksort_parallel_omp(omp_array, length);
    printf("Running MPI quicksort...\n");
    quicksort_parallel_mpi(mpi_array, length);
    printf("Running OpenMP regular sampling sort...\n");
    regular_sampling_sort_omp(regomp_array, length);
    printf("Running MPI regular sampling sort...\n");
    regular_sampling_sort_mpi(regmpi_array, length);
    printf("Done\n");

    bool equal = true;
    bool serial_ordered = true;
    bool omp_ordered = true;
    bool mpi_ordered = true;
    bool regomp_ordered = true;
    bool regmpi_ordered = true;

    for (int i = 0; i < length; ++i) {
        equal = serial_array[i] == omp_array[i] &&
            serial_array[i] == mpi_array[i] &&
            serial_array[i] == regomp_array[i] &&
            serial_array[i] == regmpi_array[i];
        if (i > 0) {
            serial_ordered = serial_array[i - 1] < serial_array[i];
            omp_ordered = omp_array[i - 1] < omp_array[i];
            mpi_ordered = mpi_array[i - 1] < mpi_array[i];
            regomp_ordered = regomp_array[i - 1] < regomp_array[i];
            regmpi_ordered = regmpi_array[i - 1] < regmpi_array[i];
        }

        if (!equal || !serial_ordered || !omp_ordered || !mpi_ordered ||
                !regomp_ordered || !regmpi_array) {
            int* error_array = serial_array;
            if (!serial_ordered) printf("Serial not ordered correctly\n");
            if (!omp_ordered) {
                error_array = omp_array;
                printf("OpenMP not ordered correctly\n");
            }
            if (!mpi_ordered) {
                error_array = mpi_array;
                printf("MPI not ordered correctly\n");
            }
            if (!regomp_ordered) {
                error_array = regomp_array;
                printf("OpenMP regular sampling not ordered correctly\n");
            }
            if (!regmpi_ordered) {
                error_array = regmpi_array;
                printf("MPI regular sampling not ordered correctly\n");
            }
            printf("Serial:\n");
            display(serial_array, length);
            printf("OpenMP:\n");
            display(omp_array, length);
            printf("MPI:\n");
            display(mpi_array, length);
            printf("OpenMP regular sort:\n");
            display(regomp_array, length);
            printf("MPI regular sort:\n");
            display(regmpi_array, length);
            printf("Error at index %d (%d)\n", i, error_array[i]);
            free(omp_array);
            free(mpi_array);
            free(regomp_array);
            free(regmpi_array);
            return 1;
        }

    }
    free(omp_array);
    free(mpi_array);
    free(regomp_array);
    return 0;
}

/**
 * Sorts an array using the provided algorithm and times it using the provided
 * timer
 */
double run_sort(int* array, int length, int runs, int seed,
        void (*sort)(int*, int), double (*timer)()) {
    double time = 0;
    for (int i = 0; i < RUNS; ++i) {
        srand(seed);
        for (int i = 0; i < length; ++i)
            array[i] = rand();

        double begin = (*timer)();
        (*sort)(array, length);
        double end = (*timer)();
        time += (double) (end - begin);
    }
    time /= runs;
    return time;
}

int main(int argc, char* argv[]) {
    int rank, nproc;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    struct argp_option options[] = {
        {"size", 'n', "NUM", 0, "Set the size of the array to generate"},
        {"seed", 's', "SEED", 0, "Set the seed of the PRNG"},
        {"test", 't', 0, 0, "Test the sorting algorithm instead of timing it"},
        {"sort", 'p', "ALGORITHM", 0, "Set the algorithm to sort with. Valid options are: serial (serial quicksort), qomp (OMP quicksort), qmpi (MPI quicksort), regomp (OMP regular sampling), regmpi (MPI regular sampling). It is only used when the program is not being tested"},
        {"cores", 'c', "CORES", 0, "Set the number of cores displayed in the output"},
        {0}
    };
    struct argp argp = { options, parse_opt };
    struct flags app_flags;
    int result = argp_parse(&argp, argc, argv, 0, 0, &app_flags);
    if (result) {
        MPI_Finalize();
        return result;
    }

    if (rank == 0) {
        // Only the main node runs the serial OpenMP implementations
        int length = app_flags.array_size;
        int* array = (int*) malloc(length * sizeof(int));
        srand(app_flags.seed);

        if (app_flags.test) {
            int result = test(array, length);
            free(array);
            MPI_Finalize();
            if (result) {
                printf("Serial and parallel arrays not sorted!\n");
                return 1;
            } else {
                printf("Serial and parallel arrays sorted correctly!\n");
                return 0;
            }
        }

        double time = run_sort(array, length, RUNS, app_flags.seed,
                app_flags.sort, app_flags.timer);
        printf("%d,%d,%s,%d,%f\n", nproc, app_flags.cores,
                getenv("OMP_NUM_THREADS"), app_flags.array_size, time);
        free(array);
    } else if (app_flags.test) {
        quicksort_parallel_mpi(0, app_flags.array_size);
        regular_sampling_sort_mpi(0, app_flags.array_size);
    } else if (app_flags.use_mpi) {
        // Other nodes just participate in the MPI implementations
        for (int i = 0; i < RUNS; ++i) {
            app_flags.sort(0, app_flags.array_size);
        }
    }
    MPI_Finalize();
    return 0;
}
