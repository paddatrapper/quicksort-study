#!/bin/bash
#SBATCH --account=icts
#SBATCH --partition=curie
#SBATCH --nodes=1 --ntasks=4
#SBATCH --time=15:00
#SBATCH --job-name="SortOMP4"
#SBATCH --mail-user=RBBKYL001@myuct.ac.za
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --output=omp4.csv
#SBATCH --error=omp4.err

INPUTS=( 100, 1000, 10000, 100000, 1000000 )
module load mpi/openmpi-4.0.1

cd ~/quicksort-study/
make >/dev/null

echo "#OMP4"
echo "num_nodes,num_cores,num_threads,n,time"
export OMP_NUM_THREADS=2
for j in {1..10}; do
    for i in "${INPUTS[@]}"; do
        ./sort -n $i -p omp -c $SLURM_TASKS_PER_NODE
    done
done

export OMP_NUM_THREADS=4
for j in {1..10}; do
    for i in "${INPUTS[@]}"; do
        ./sort -n $i -p omp -c $SLURM_TASKS_PER_NODE
    done
done

export OMP_NUM_THREADS=8
for j in {1..10}; do
    for i in "${INPUTS[@]}"; do
        ./sort -n $i -p omp -c $SLURM_TASKS_PER_NODE
    done
done
