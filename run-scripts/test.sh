#!/bin/bash
#SBATCH --account=icts
#SBATCH --partition=curie
#SBATCH --nodes=4 --ntasks=4
#SBATCH --time=00:20
#SBATCH --job-name="Sort"
#SBATCH --mail-user=RBBKYL001@myuct.ac.za
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --output=test.out

module load mpi/openmpi-4.0.1

cd ~/quicksort-study/
make >/dev/null

mpirun -n 4 --quiet ./sort -n 50 -t
mpirun -n 4 --quiet ./sort -n 100 -t
mpirun -n 4 --quiet ./sort -n 500 -t
mpirun -n 4 --quiet ./sort -n 142 -t
