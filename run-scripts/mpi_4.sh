#!/bin/bash
#SBATCH --account=icts
#SBATCH --partition=curie
#SBATCH --nodes=4 --ntasks=4
#SBATCH --time=20:00
#SBATCH --job-name="SortMPI4"
#SBATCH --mail-user=RBBKYL001@myuct.ac.za
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --output=mpi4.csv
#SBATCH --error=mpi4.err

INPUTS=( 100, 1000, 10000, 100000, 1000000 )
module load mpi/openmpi-4.0.1

cd ~/quicksort-study/
make >/dev/null

echo "#MPI4"
echo "num_nodes,num_cores,num_threads,n,time"
for j in {1..10}; do
    for i in "${INPUTS[@]}"; do
        mpirun -n 4 --quiet ./sort -n $i -p mpi -c $SLURM_TASKS_PER_NODE
    done
done
