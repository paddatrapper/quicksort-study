#!/bin/bash
#SBATCH --account=icts
#SBATCH --partition=curie
#SBATCH --nodes=1 --ntasks=1
#SBATCH --time=05:00
#SBATCH --job-name="SortSerial"
#SBATCH --mail-user=RBBKYL001@myuct.ac.za
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --output=serial.csv
#SBATCH --error=serial.err

INPUTS=( 100, 1000, 10000, 100000, 1000000 )
module load mpi/openmpi-4.0.1

cd ~/quicksort-study/
make >/dev/null

echo "#SERIAL"
echo "num_nodes,num_cores,num_threads,n,time"
for j in {1..10}; do
    for i in "${INPUTS[@]}"; do
        ./sort -n $i -p serial -c $SLURM_TASKS_PER_NODE
    done
done

