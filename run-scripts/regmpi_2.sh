#!/bin/bash
#SBATCH --account=icts
#SBATCH --partition=curie
#SBATCH --nodes=2 --ntasks=2
#SBATCH --time=10:00
#SBATCH --job-name="SrtRMPI2"
#SBATCH --mail-user=RBBKYL001@myuct.ac.za
#SBATCH --mail-type=BEGIN,END,FAIL
#SBATCH --output=rmpi2.csv
#SBATCH --error=rmpi2.err

INPUTS=( 100, 1000, 10000, 100000, 1000000 )
module load mpi/openmpi-4.0.1

cd ~/quicksort-study/
make >/dev/null

echo "#RMPI2"
echo "num_nodes,num_cores,num_threads,n,time"
for j in {1..10}; do
    for i in "${INPUTS[@]}"; do
        mpirun -n 2 --quiet ./sort -n $i -p regmpi -c $SLURM_TASKS_PER_NODE
    done
done
